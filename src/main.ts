import Vue from 'vue';
import App from './App.vue';
import router from './router';
import './registerServiceWorker';

// @ts-ignore
import { MdButton, MdField } from 'vue-material/dist/components';
import 'vue-material/dist/vue-material.min.css';

Vue.config.productionTip = false;

Vue.use(MdButton);
Vue.use(MdField);

new Vue({
    router,
    render: (h) => h(App)
}).$mount('#app');
