export interface CamPlace {
  cams: Cam[];
  name: string;
  prov: string;
}

export interface Cam {
  name: string;
  url: string;
}
